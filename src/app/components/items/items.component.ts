import {Component, OnInit} from '@angular/core';
import {IItem} from "../../models/IItem";
import {DataService} from "../../services/data.service";

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  item: IItem = {
    id: 0,
    name: '',
    type: ''
  };

  items: IItem[] = [];
  tableMode = true;

  constructor(private dataService: DataService) {
  }

  ngOnInit() {
    this.loadItems();
  }

  add(item: IItem) {
    item.id = +1;
    this.items.push(item);
    this.tableMode = false;
  }

  loadItems() {
    this.dataService.getItems(0, 5)
      .subscribe((data: IItem[]) => this.items = data);
    console.log(this.items);
  }

  editItem() {

  }

  save() {

  }

  cancel() {

  }

  delete() {

  }

}
