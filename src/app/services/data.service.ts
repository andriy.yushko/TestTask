import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {IItem} from '../models/IItem';

@Injectable()
export class DataService {

  constructor(private _http: HttpClient) {
  }

  private url = 'http://localhost:63143/api/Items';

  getItems(skip, range) {
    const httpParams = new HttpParams();
    httpParams.append('skip', skip);
    httpParams.append('range', range);
    return this._http.get(this.url, {params: httpParams});
  }

  addItem(item: IItem) {
    this._http.post(this.url, item);
  }

  // updateItem(item: IItem) {
  //   return this._http.put(this.url + '/' + item.id, item);
  // }
  //
  // deleteItem(id: number) {
  //   return this._http.delete(this.url + '/' + id);
  // }
}
